-r base.txt

coveralls==1.1
flake8==2.4.1
pytest-cov==2.2.0
pytest==2.7.2
redis==2.10.5
tox==2.1.1
